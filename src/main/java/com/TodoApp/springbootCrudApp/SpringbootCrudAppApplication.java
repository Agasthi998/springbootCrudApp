package com.TodoApp.springbootCrudApp;

import com.TodoApp.springbootCrudApp.repository.TodoRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableMongoAuditing
@SpringBootApplication(scanBasePackages = "com.TodoApp.springbootCrudApp")
@EnableMongoRepositories(basePackageClasses ={TodoRepository.class})
public class SpringbootCrudAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootCrudAppApplication.class, args);
	}

}
