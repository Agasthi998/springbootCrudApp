package com.TodoApp.springbootCrudApp.config;

import com.TodoApp.springbootCrudApp.constant.Constants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.client.LinkDiscoverer;
import org.springframework.hateoas.client.LinkDiscoverers;
import org.springframework.hateoas.mediatype.collectionjson.CollectionJsonLinkDiscoverer;
import org.springframework.plugin.core.SimplePluginRegistry;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;
import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket aggregationService() {

        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.basePackage(Constants.SwaggerConfig.BASE_PACKAGE)).paths(regex(Constants.SwaggerConfig.PATH_REGEX))
                .build().apiInfo(metaData());
    }

    private ApiInfo metaData() {

        return new ApiInfo(Constants.SwaggerConfig.TITLE, Constants.SwaggerConfig.DESCRIPTION,
                Constants.SwaggerConfig.APP_VERSION_VALUE, Constants.SwaggerConfig.TERMS_OF_SERVICE_URL,
                new Contact(Constants.SwaggerConfig.CONTACT_NAME, Constants.SwaggerConfig.CONTACT_URL,
                        Constants.SwaggerConfig.CONTACT_EMAIL),
                Constants.SwaggerConfig.LICENSE, Constants.SwaggerConfig.LICENSE_URL, new ArrayList<>());
    }

    @Bean
    public LinkDiscoverers discoverers() {
        List<LinkDiscoverer> plugins = new ArrayList<>();
        plugins.add(new CollectionJsonLinkDiscoverer());
        return new LinkDiscoverers(SimplePluginRegistry.create(plugins));

    }
}
