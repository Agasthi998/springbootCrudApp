package com.TodoApp.springbootCrudApp.constant;

public class Api {
    public static final String API_BASE_URL = "/api";

    public static final String TODO_PATH = "/todos";

    public static final String TODO_ID_PATH = "/{id}";

    public static final String TODO_ID = "e948717e-c4d7-490b-a73a-fcedb10960db";

}
