package com.TodoApp.springbootCrudApp.constant;

public class Constants {
    public static final String MONGODB_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    private Constants() {
        // This is to make sure that no one will instantiate this class
    }

    public static class TodoService {
        public static final String TODO_COLLECTION = "todos";


        private TodoService() {
            // This is to make sure that no one will instantiate this class
        }
    }

    public class SwaggerConfig {
        public static final String BASE_PACKAGE = "com.TodoApp.springbootCrudApp.controller";
        public static final String PATH_REGEX = "/api.*";
        public static final String TITLE = "SpringBoot | CRUD Service";
        public static final String DESCRIPTION = "RESTful API";
        public static final String TERMS_OF_SERVICE_URL = "";
        public static final String CONTACT_NAME = "Agasthi";
        public static final String CONTACT_URL = "https://www.pearson.com/us/higher-education/products-services-teaching/learning-engagement-tools/pearson-prep/for-students.html";
        public static final String CONTACT_EMAIL = "crudApp.com";
        public static final String LICENSE = "Apache License Version 2.0";
        public static final String LICENSE_URL = "https://www.apache.org/licenses/LICENSE-2.0";
        public static final String APP_VERSION_VALUE = "V-4.5.2";
    }
}
