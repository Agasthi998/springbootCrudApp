package com.TodoApp.springbootCrudApp.constant;

public class TestConstants {
    public static final String CREATE_NEW_TODO_TO_TEST = "superman";
    public static final String CREATE_NEW_COMPLETE_STATUS_TO_TEST = "complete";
}
