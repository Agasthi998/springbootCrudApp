package com.TodoApp.springbootCrudApp.controller;


import com.TodoApp.springbootCrudApp.constant.Api;
import com.TodoApp.springbootCrudApp.domain.Todo;
import com.TodoApp.springbootCrudApp.service.TodoService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(Api.API_BASE_URL + Api.TODO_PATH)
public class TodoController {
    private static final Logger LOGGER = LoggerFactory.getLogger(TodoController.class);
    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }


    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses({@ApiResponse(code = 200, message = "Successful retrieval of todo tasks"),
            @ApiResponse(code = 400, message = "Bad Request, validation error"),
            @ApiResponse(code = 401, message = "Cannot find a valid token")})
    public ResponseEntity<List<Todo>> getAllTodos() {
        LOGGER.info("TodoController: getAllTodo");
            return new ResponseEntity<>(todoService.getAllTodos(), HttpStatus.OK);
    }

      @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
      @ApiResponses({@ApiResponse(code = 201, message = "Successful creation of Todo task."),
              @ApiResponse(code = 304, message = "Resource not found"),
              @ApiResponse(code = 401, message = "Cannot find a valid token")})
      public ResponseEntity<?> createTodo(@RequestBody Todo todos){
          LOGGER.info("TodoController: createTodo");
            return new ResponseEntity<Todo>(todoService.createTodo(todos), HttpStatus.OK);
      }

    @GetMapping(path = Api.TODO_ID_PATH, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses({@ApiResponse(code = 200, message = "Successful read of a todo."),
            @ApiResponse(code = 400, message = "Bad Request, validation error"),
            @ApiResponse(code = 401, message = "Cannot find a valid token"),
            @ApiResponse(code = 404, message = "Resource not found")})
    public ResponseEntity<Todo> getTodoById(@PathVariable String id){
        return new ResponseEntity<>(todoService.getTodoById(id), HttpStatus.OK);
    }


    @PutMapping(path = Api.TODO_ID_PATH, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses({@ApiResponse(code = 200, message = "Successful update of todo tasks."),
            @ApiResponse(code = 400, message = "Bad Request, validation error"),
            @ApiResponse(code = 401, message = "Cannot find a valid token"),
            @ApiResponse(code = 404, message = "Resource not found")})
    public ResponseEntity<Todo> updateTodo(@PathVariable("id") String id, @RequestBody Todo todos){
        LOGGER.info("TodoController: updateTodo");
        return new ResponseEntity<>(todoService.updateTodo(id, todos), HttpStatus.OK);
    }

    @DeleteMapping(path = Api.TODO_ID_PATH, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Successfully deleted all todo tasks"),
            @ApiResponse(code = 401, message = "You are not authorized to do the operation"),
            @ApiResponse(code = 404, message = "todo task not found for the given deck ID")
    })
    public void deleteTodo(@PathVariable String id){
        LOGGER.info("TodoController: deleteTodo");
            todoService.deleteTodo(id);

    }

}
