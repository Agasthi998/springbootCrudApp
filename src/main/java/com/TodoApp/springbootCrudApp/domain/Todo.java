package com.TodoApp.springbootCrudApp.domain;

import com.TodoApp.springbootCrudApp.constant.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.Date;


@Document(collection = Constants.TodoService.TODO_COLLECTION )
public class Todo {

    @Id
    private String id;
    private String todo;
    private String completed;

    @CreatedDate
    @JsonFormat(pattern = Constants.MONGODB_DATE_FORMAT)
    private Date createdAt;

    @CreatedDate
    @JsonFormat(pattern = Constants.MONGODB_DATE_FORMAT)
    private Date updatedAt;

    public Todo() {

    }

    public Todo(String id, String todo, String completed, Date createdAt, Date updatedAt) {
        this.id = id;
        this.todo = todo;
        this.completed = completed;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTodo(String todo) {
        this.todo = todo;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public String getId() {
        return id;
    }

    public String getTodo() {
        return todo;
    }

    public String getCompleted() {
        return completed;
    }

    public String errorMessage;

    public Todo(String errorMessage){
        this.errorMessage = errorMessage;
    }
    public String getErrorMessage(){
        return errorMessage;
    }

}
