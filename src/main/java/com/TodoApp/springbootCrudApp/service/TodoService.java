package com.TodoApp.springbootCrudApp.service;

import com.TodoApp.springbootCrudApp.domain.Todo;

import java.util.List;

public interface TodoService {
    /**
     * Creates a new Todo resource in the database
     * @param todos
     * @return created todo
     */
    Todo createTodo(Todo todos);

    /**
     * Update an already available todo
     * @param id ObjectId of the todo document
     * @param todos
     * @return updated todo
     */
    Todo updateTodo(String id, Todo todos);

    /**
     * Delete todo.
     * @param id the todo id
     */
    Todo deleteTodo(String id);

    /**
     * Retrieves all the active todo tasks
     * @return active todos
     */
    List<Todo> getAllTodos();

    /**
     * Retrieve a todo by todo Id
     * @param id todo Id
     * @return todo
     */
    Todo getTodoById(String id);

}