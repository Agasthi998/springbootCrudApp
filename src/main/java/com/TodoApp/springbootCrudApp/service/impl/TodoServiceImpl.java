package com.TodoApp.springbootCrudApp.service.impl;


import com.TodoApp.springbootCrudApp.constant.ExceptionMessages;
import com.TodoApp.springbootCrudApp.domain.Todo;
import com.TodoApp.springbootCrudApp.repository.TodoRepository;
import com.TodoApp.springbootCrudApp.service.TodoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Implementation of the TodoService
 *
 * @see TodoService
 */

@Service
public class TodoServiceImpl implements TodoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TodoServiceImpl.class);
    private final TodoRepository todoRepository;

    /**
     * Instantiates a new todo service
     * @param todoRepository the todo repository
     */

    public TodoServiceImpl(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    @Override
    public Todo createTodo(Todo todos) {
        LOGGER.info("TodoServiceImpl : createTodo");
        return todoRepository.save(todos);
    }


    @Override
    public Todo updateTodo(String id, Todo todos) {
        LOGGER.info("TodoServiceImpl : updateTodo");
        Optional<Todo> todosOptional = todoRepository.findById(id);
        if(todosOptional.isPresent()){
            LOGGER.info("TodoServiceImpl : update Todo");
            Todo todoSave = todosOptional.get();
            todoSave.setTodo(todos.getTodo() != null ? todos.getTodo() : todoSave.getTodo());
            todoSave.setCompleted(todos.getCompleted() != null ? todos.getCompleted() : todoSave.getCompleted());
            todoSave.setUpdatedAt(new Date());
            return todoRepository.save(todoSave);
        }else{
            LOGGER.error("TodoServiceImpl : update Todo: {}", ExceptionMessages.TODO_DOES_NOT_EXISTS);
            //throw new NotFoundException(ExceptionMessage.TODO_DOES_NOT_EXISTS , HttpStatus.NOT_FOUND);
            //return null;
            return  new Todo(ExceptionMessages.TODO_DOES_NOT_EXISTS);
        }
    }


@Override
public Todo deleteTodo(String id) {
    Optional<Todo> todosOptional = todoRepository.findById(id);
    if(todosOptional.isPresent()){
        LOGGER.info("TodoServiceImpl : delete specific id todo in the list");
        Todo deleteTodo = todosOptional.get();
        todoRepository.delete(deleteTodo);
        return  deleteTodo;
    }else{
        LOGGER.error("TodoServiceImpl : delete Todo : {}" , ExceptionMessages.TODO_DOES_NOT_EXISTS);
        //throw new NotFoundException(ExceptionMessage.TODO_DOES_NOT_EXISTS,  HttpStatus.NOT_FOUND);
        //return null;
        return  new Todo(ExceptionMessages.TODO_DOES_NOT_EXISTS);
    }
}

    @Override
    public List<Todo> getAllTodos() {
        LOGGER.info("TodoServiceImpl : getAllTodos");
        return todoRepository.findAll();
    }

    @Override
    public Todo getTodoById(String id) {
        LOGGER.info("TodoServiceImpl : getTodoById");
        Optional<Todo> todoOptional = todoRepository.findById(id);
        if(todoOptional.isPresent()){
            LOGGER.info("TodoServiceImpl : get Todo");
            return todoOptional.get();
        } else {
            return  new Todo(ExceptionMessages.TODO_DOES_NOT_EXISTS);
        }
    }

}

