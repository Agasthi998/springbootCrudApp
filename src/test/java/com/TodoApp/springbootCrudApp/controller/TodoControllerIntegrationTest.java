//package com.TodoApp.springbootCrudApp.controller;
//
//import com.TodoApp.springbootCrudApp.SpringbootCrudAppApplication;
//import com.TodoApp.springbootCrudApp.constant.TestConstants;
//import com.TodoApp.springbootCrudApp.domain.Todo;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.FixMethodOrder;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.junit.runners.MethodSorters;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.web.server.LocalServerPort;
//import org.springframework.core.ParameterizedTypeReference;
//import org.springframework.http.*;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.web.client.RestTemplate;
//import java.util.List;
//import java.util.UUID;
//import static com.TodoApp.springbootCrudApp.constant.Api.*;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT, classes = SpringbootCrudAppApplication.class)
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
//public class TodoControllerIntegrationTest {
//
//    private String host = "http://localhost";
//    private String urlTodo = "";
//
//    @LocalServerPort
//    private String port;
//
//    private RestTemplate restTemplate;
//    private HttpHeaders httpHeaders;
//    private Todo todo;
//    private String todoId;
//    private String id = TODO_ID;
//
//    /**
//     * define_sample_data_to_test
//     */
//    @Before
//    public void setup(){
//        restTemplate = new RestTemplate();
//        httpHeaders = new HttpHeaders();
//        httpHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
//        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
//        host = host.concat(":" +port);
//        urlTodo = urlTodo.concat(host + API_BASE_URL + TODO_PATH); //http://localhost:5500/api/todos/
//    }
//
//    /**
//     * Create_Todo_Test
//     */
//    @Test
//    public void createTodoTest(){
//        String url = urlTodo;
//        Todo todo = new Todo();
//        todo.setId(UUID.randomUUID().toString());
//        todo.setTodo(TestConstants.CREATE_NEW_TODO_TO_TEST);
//        todo.setCompleted(TestConstants.CREATE_NEW_COMPLETE_STATUS_TO_TEST);
//        HttpEntity<Todo> httpEntity = new HttpEntity<>(todo, httpHeaders);
//        ResponseEntity<Todo> responseEntity = restTemplate.exchange(url, HttpMethod.POST, httpEntity, Todo.class);
//        Assert.assertNotNull(responseEntity.getBody());
//        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
//    }
//
//    /**
//     * View_All_Todo_Test
//     */
//
//    @Test
//    public void viewAllTodoList(){
//        String url = urlTodo.concat("");
//        HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
//        ResponseEntity<List<Todo>> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, new ParameterizedTypeReference<List<Todo>>() {
//        });
//        Assert.assertNotNull(responseEntity.getBody());
//        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
//    }
//
//    /**
//     * Get Todo_by_id_test
//     */
//
//    @Test
//    public void getTodoByIdTest() {
//        String url = urlTodo.concat(id);
//        HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
//        ResponseEntity<Todo> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Todo.class);
//
//        Assert.assertNotNull(responseEntity.getBody());
//        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
//    }
//
//    /**
//     * Delete_Todo_Test
//     */
//    @Test
//    public void deleteTodoByIdTest(){
//        String url = urlTodo.concat(id);
//        HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
//        ResponseEntity<Void> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, httpEntity, Void.class);
//        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
//    }
//
//    /**
//     * Update_Todo_Test
//     */
//    @Test
//    public void updateTodoTest(){
//        String url = urlTodo.concat(id);
//        Todo todo = new Todo();
//        todo.setTodo("Create todo");
//        todo.setCompleted("pending");
//        HttpEntity<Todo> httpEntity = new HttpEntity<>(todo, httpHeaders);
//        ResponseEntity<Todo> responseEntity = restTemplate.exchange(url, HttpMethod.PUT,httpEntity,Todo.class);
//        Assert.assertNotNull(responseEntity.getBody());
//        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
//    }
//
//}
