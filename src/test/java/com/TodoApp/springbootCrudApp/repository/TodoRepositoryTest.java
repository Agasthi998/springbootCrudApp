package com.TodoApp.springbootCrudApp.repository;

import com.TodoApp.springbootCrudApp.domain.Todo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class TodoRepositoryTest {
    @Autowired
    private TodoRepository todoRepository;

    private Todo todo;

    @BeforeEach
    void initUseCase(){
        todo = new Todo();
        todo.setId("test_id");
        todo.setTodo("test_title");
        todo.setCompleted("test_description");
        todoRepository.save(todo);
    }
    @AfterEach
    void tearDown(){

        todoRepository.deleteById("test1");
    }

    @Test
    void saveUserTest() {

        assertEquals(todo, todoRepository.save(todo));
    }

    @Test
    void findAllByTodo() {
        assertNotNull(todoRepository.findById("test_id"));
    }

    @Test
    void deleteById() {
        todoRepository.deleteById("test_id");
        assertNull(todoRepository.findById("test_id").orElse(null));
    }

    @Test
    void findUsersByIdTest() {

        assertNotNull(todoRepository.findById("test_id"));
    }

    @Test
    void findAllUsersTest() {

        assertFalse((todoRepository.findAll()).isEmpty());
    }

}
