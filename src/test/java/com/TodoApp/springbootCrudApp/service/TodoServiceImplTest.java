//package com.TodoApp.springbootCrudApp.service;
//
//import com.TodoApp.springbootCrudApp.constant.ExceptionMessages;
//import com.TodoApp.springbootCrudApp.domain.Todo;
//import com.TodoApp.springbootCrudApp.repository.TodoRepository;
//import com.TodoApp.springbootCrudApp.service.impl.TodoServiceImpl;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.Mock;
//import org.springframework.boot.test.context.SpringBootTest;
//import java.util.Arrays;
//import java.util.Optional;
//import static org.junit.jupiter.api.Assertions.assertAll;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.Mockito.doReturn;
//
//
//@SpringBootTest
//public class TodoServiceImplTest {
//
//    @Mock
//    private TodoRepository todoRepository;
//
//    private Todo todo;
//
//    private TodoServiceImpl todoServiceImpl;
//
//    /**
//     * define_sample_data_to_test
//     */
//    @BeforeEach
//    void initUseCase(){
//        todoServiceImpl = new TodoServiceImpl(todoRepository);
//        todo = new Todo();
//        todo.setId("test_id");
//        todo.setTodo("test_title");
//        todo.setCompleted("Incomplete");
//    }
//    /**
//     * Test_created_new_todo
//     */
//
//    @Test
//    public void saveTodo(){
//        doReturn(todo).when(todoRepository).save(any(Todo.class));
//        Assertions.assertEquals(todo, todoServiceImpl.createTodo(todo));
//        Assertions.assertEquals(todo.getId(),"test_id");
//    }
//
//    /**
//     * Test_get_all_todo
//     */
//    @Test
//    void getAllTodo(){
//        doReturn(Arrays.asList(todo)).when(todoRepository).findAll();
//        Assertions.assertEquals(Arrays.asList(todo), todoServiceImpl.getAllTodos());
//    }
//
//    /**
//     * Test_get_by_id_todo
//     */
//    @Test
//    void getTodoById(){
//        doReturn(Optional.of(todo)).when(todoRepository).findById("Test_id");
//        Assertions.assertEquals(todo, todoServiceImpl.getTodoById("Test_id"));
//        Assertions.assertEquals(todo.getTodo(), "test_title");
//    }
//
//    /**
//     * Test_invalid_get_by_id_todo
//     */
//    @Test
//    void getTodoByIdNotExistTest(){
//        doReturn(Optional.ofNullable(null)).when(todoRepository).findById("test_id");
//        Todo exceptionMessage = todoServiceImpl.getTodoById("test_id" );
//        Assertions.assertEquals(ExceptionMessages.TODO_DOES_NOT_EXISTS, exceptionMessage.getErrorMessage());
//        //Assertions.assertEquals(todo.getTodo(),"test_title");
//    }
//
//    /**
//     * Test_delete_id_todo
//     */
//    @Test
//    void deleteByIdTest(){
//        doReturn(Optional.of(todo)).when(todoRepository).findById("test_id");
//        Assertions.assertEquals(todo, todoServiceImpl.deleteTodo("test_id"));
//    }
//
//    /**
//     * Test_delete_invalid_id_todo
//     */
//    @Test
//    void deleteByIdNotExistTest(){
//        doReturn(Optional.ofNullable(null)).when(todoRepository).findById("test_id");
//        Todo exceptionMessage = todoServiceImpl.deleteTodo("test_id" );
//        Assertions.assertEquals(ExceptionMessages.TODO_DOES_NOT_EXISTS, exceptionMessage.getErrorMessage());
//        // Assertions.assertEquals(null, todoServiceImpl.deleteTodo("test_id"));
//    }
//
//    /**
//     * Test_update_todo
//     */
//    @Test
//    void updateTodoById(){
//        doReturn(Optional.of(todo)).when(todoRepository).findById("test_id");
//        doReturn(todo).when(todoRepository).save(any(Todo.class));
//        Todo response = todoServiceImpl.updateTodo("test_id" ,todo);
//        assertAll("Should return Todo type object with todo's data",
//                () -> Assertions.assertEquals("test_id", response.getId()),
//                () -> Assertions.assertEquals("test_id", todo.getId())
//        );
//    }
//
//    /**
//     * Test_invalid_update_todo_id
//     */
//    @Test
//    void updateTodoByIdNotExistTest(){
//        doReturn(Optional.ofNullable(null)).when(todoRepository).findById("test_id");
//        Todo exceptionMessage = todoServiceImpl.updateTodo("test_id" ,todo);
//        Assertions.assertEquals(ExceptionMessages.TODO_DOES_NOT_EXISTS, exceptionMessage.getErrorMessage());
//
//    }
//}
